import Graph from './graph'
import Vue from 'vue'

import App from './App.vue';


Graph.getGraph().then((graph: Graph): void => {
    new Vue({ render: (createElement): Vue => createElement(App, {
        props: {
            graph: graph,
        },
    }) }).$mount('#app');
})
