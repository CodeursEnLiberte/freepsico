import Papa, {ParseError } from 'papaparse'

const stationsUrl = "./noeuds_alpha_pcc.txt"
const edgesUrl = "./arcs_vit_en.csv"

interface RawEdge {
  ido: number;
  idf: number;
  longueur: number;
  vmax: number;
  nrj: number;
}

class Edge {
  from: number;
  to: number;
  length: number;
  maxSpeed: number;
  electrified: boolean;
  duration: number;

  constructor(row: RawEdge){
    this.from = row.ido;
    this.to = row.idf;
    this.length = row.longueur;
    this.maxSpeed = row.vmax;
    this.electrified = row.nrj === 1;
    this.duration = this.length * 60 / this.maxSpeed;
  }

  accessible(elec: boolean, avoidLGV: boolean): boolean {
    return (this.electrified || !elec) && (this.maxSpeed < 250 || !avoidLGV)
  }
}

interface RawStation {
  'Id epsico': number;
  'libelle epsico': string;
  RGI_XX: number;
  RGI_YY: number;
}

class Station {
  id: number;
  name: string;
  x: number;
  y: number;

  constructor(row: RawStation) {
    this.id = row['Id epsico'];
    this.name = row['libelle epsico'];
    this.x = row.RGI_XX;
    this.y = row.RGI_YY;
  }
}

function getCsv<T>(c: {new(row: RawEdge | RawStation): T }, url: string, delimiter: string): Promise<T[]> {
  const result: T[] = [];
  return new Promise((resolve, error) => Papa.parse(url, {
    delimiter,
    download: true,
    header: true,
    dynamicTyping: true,
    step: (row) => { result.push(new c(row.data)) },
    error(errors: ParseError) {
      error(errors)
    },
    complete() {
      resolve(result)
    },
  }))
}

interface Coordinate {
  x: number;
  y: number;
}

class Graph {
  stations: Map<number, Station>;
  edges: Edge[];
  minX: number;
  maxX: number;
  minY: number;
  maxY: number;

  constructor(stations: Station[], edges: Edge[]) {
    this.stations = new Map();
    this.edges = edges;
    this.minX = Number.MAX_VALUE;
    this.maxX = Number.MIN_VALUE;
    this.minY = Number.MAX_VALUE;
    this.maxY = Number.MIN_VALUE;
    for (const station of stations) {
      this.stations.set(station.id, station)
      if(!isNaN(station.x) && !isNaN(station.y)) {
        this.minX = Math.min(this.minX, station.x)
        this.maxX = Math.max(this.maxX, station.x)
        this.minY = Math.min(this.minY, station.y)
        this.maxY = Math.max(this.maxY, station.y)
      }
    }
  }

  proj(station: Station, width: number): Coordinate {
    const pixelsPerMeter = width / (this.maxX - this.minX);
      return {
        x: (station.x - this.minX) * pixelsPerMeter,
        y: width - (station.y - this.minY) * pixelsPerMeter,
      }
  }
}

async function getGraph(): Promise<Graph> {
  const stations = getCsv(Station, stationsUrl, "\t")
  const edges = getCsv(Edge, edgesUrl, ";")

  const values = await Promise.all([stations, edges]);
  return new Graph(values[0], values[1]);
}

function initFloatArray(size: number, origin: number): Float64Array {
  const result = new Float64Array(size)
  result.fill(Infinity)
  result[origin] = 0
  return result
}

class RoutingStruct {
  /* number are float64 in JS
     Having Float64 arrays lead to unexpected behaviour when comparing things like Float32 < number
  */
  nodesCount: number;
  dist: Float64Array;
  pred: Uint16Array;
  duration: Float64Array;
  distanceElec: Float64Array;
  distanceLGV: Float64Array;

  constructor(nodesCount: number, origin: number) {
    this.nodesCount = nodesCount
    this.pred = new Uint16Array(nodesCount)
    this.dist = initFloatArray(nodesCount, origin)
    this.duration = initFloatArray(nodesCount, origin)
    this.distanceElec = initFloatArray(nodesCount, origin)
    this.distanceLGV = initFloatArray(nodesCount, origin)

    for (let i = 0; i < nodesCount; i++) {
      this.pred[i] = i
    }
  }

  relax(edge: Edge, reverse: boolean): boolean {
    const from = reverse? edge.from: edge.to;
    const to = reverse? edge.to: edge.from;

    if (this.dist[from] + edge.length < this.dist[to]) {
      this.pred[to] = from
      this.dist[to] = this.dist[from] + edge.length
      this.duration[to] = this.duration[from] + edge.duration
      this.distanceLGV[to] = this.distanceLGV[from] + (edge.maxSpeed > 250 ? edge.length : 0)
      this.distanceElec[to] = this.distanceElec[from] + (edge.electrified ? edge.length : 0)
      return true
    } else {
      return false
    }
  }

  route(to: number): number[] {
    const result = []
    let current = to
    do {
      result.push(current)
      current = this.pred[current]
    } while (this.pred[current] != current)

    return result
  }
}

function bellman(graph: Graph, from: number, to: number, elec: boolean, avoidLGV: boolean): object {
  let maxNode = 0;
  graph.stations.forEach((_v: Station, index: number) => maxNode = Math.max(maxNode, index))
  const structs = new RoutingStruct(maxNode, from)

  for (let i = 0; i < maxNode; i++) {
    let improved = false;
    for (const edge of graph.edges) {
      if (edge.accessible(elec, avoidLGV)) {
        // We need those two calls to happen, so we can’t use `||`
        if (structs.relax(edge, false)) {improved = true}
        if (structs.relax(edge, true)) {improved = true}
      }
    }
    // If no edge improved anything during this iteration, we stop
    if (!improved) {
      break;
    }
  }

  return {
    nodes: structs.route(to),
    distance: structs.dist[to],
    duration: structs.duration[to],
    distanceLGV: structs.distanceLGV[to],
    distanceElec: structs.distanceElec[to],
  }
}

export default {
    getGraph,
    bellman,
    Graph,
}
